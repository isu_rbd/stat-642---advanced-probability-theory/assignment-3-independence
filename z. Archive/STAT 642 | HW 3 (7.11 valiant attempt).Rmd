---
title: "STAT 642 | HW 3"
author: "Ricardo Batista"
date: "2/4/2020"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Question 1

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\mathbb{E}\left[\vert X \vert \right] < \infty \implies \sum_{n = 1}^\infty P(\vert X \vert > \varepsilon n) < \infty \text{ for every } \varepsilon > 0}$:

&nbsp;&nbsp;&nbsp;&nbsp; Let $\varepsilon > 0$ be given. Note that, by Markov's inequality we have

$$
P(\vert X \vert > \varepsilon n) \leq \frac{\mathbb{E}[\vert X \vert]}{\varepsilon n}
$$

and $\frac{\mathbb{E}[\vert X \vert]}{\varepsilon n} \rightarrow 0$. That is, $\frac{\mathbb{E}[\vert X \vert]}{\varepsilon n}$ is a convergent sequence so

[Convergent sequence doesn't imply convergent series]

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\sum_{n = 1}^\infty P(\vert X \vert > \varepsilon n) < \infty \text{ for every } \varepsilon > 0 \implies \mathbb{E}\left[\vert X \vert \right] < \infty}$:

&nbsp;&nbsp;&nbsp;&nbsp; 




